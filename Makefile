knapsack: main.o max.o
	gcc -g -o knapsack main.o max.o
main.o: main.c main.h
	gcc -g -c main.c
max.o: max.c
	gcc -g -c max.c
clean:
	rm knapsack main.o max.o 
