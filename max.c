#include"main.h"

//max value takes the capacity and array of structures
//returns the structure containing the max value and items to attain max value
//
//uses dynamic programming and recursion to find max val

struct answer maxarray[1025];

struct answer max(int cap,struct list allitems[128])
{
	struct answer best;						//declare and init struct answer best
	best.max=0;
	//printf("%d\n",i);
//	for(int k=0;k<=cap;k++)
//	{
//		struct answer maxarray[k]=0;
//	}

//	memset(maxarray,0,sizeof maxarray);

	for(int k=0;k<=128;k++)					//init best.items array
	{
		best.items[k]=0;
	}
	if(maxarray[cap].max != 0) return maxarray[cap];

	if(cap <= 0) return best;				//if cap <=0 return 0


	for(int j=0;j<=i-1;j++)					//for all items(i is all items in list) 
	{
		if(allitems[j].wt <= cap)			//if legal weight
		{
			struct answer try = max(cap-allitems[j].wt,allitems);	//try by recursive call to max with cap-item weight

			if(try.max + allitems[j].val > best.max)				//if new max is higher then prev max
			{
				best=try;											//set new max
				best.max=try.max + allitems[j].val;
				++best.items[j];									//increment item counter
			}
		}
	}
	maxarray[cap]=best;
	return best;							//return max value and item counts
}

