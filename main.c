#include"main.h"

//PA5 Braden Reames
//Knapsack Program
//
//Command Line: "knapsack" "integer capacity" "file"
//
//Program finds the maximum value of items that can fit in a knapsack with given capacity
int main(int argc, char **argv)
{
	FILE *file;							//declare vars
	char name[32],buffer[120];
	int wt,val,cap,test;
	struct list allitems[128];			//declare array of struct list

	if(argc != 3)						//error condition
	{
		printf("Usage knapsack int file\n");
		return 1;
	}

	if(argc == 3)
	{	
		if(sscanf(argv[1],"%i",&cap) != 1) 		//parse integer argument for cap			
		{
			printf("Enter an integer for capacity.\n");
			return 1;
		}
		sscanf(argv[1],"%i",&test);
		cap=test;
	//	printf("%d\n",cap);
		file=fopen(argv[2],"r");				//open file and read
	}

	if(file == NULL)						//error condition
	{
		printf("Cannot open %s\n",argv[2]);
		return 1;
	}

	i=0;										//first item in file

	while(fgets(buffer,120,file) != NULL)		//while in the file
	{
		sscanf(buffer,"%s%d%d",name,&val,&wt);	//parse lines into vars
		strcpy(allitems[i].name,name);			//copy name
		allitems[i].val=val;					//set value
		allitems[i].wt=wt;						//set weight
//		printf("%d %d %s",allitems[i].wt,allitems[i].val,allitems[i].name);
		++i;									//increment i(to keep track of the amount of diff items)
	}

	struct answer best=max(cap,allitems);		//call max function and return struct answer

	printf(" Weight  Value  Name\n");			
	printf(" ------  -----  ----\n");
	for(int j=0;j<=i-1;j++)						//iterate though and print item info
	{
		printf("    %d     %d     %s\n",allitems[j].wt,allitems[j].val,allitems[j].name);
	}

	printf(" -------------------\n");
	printf("Bag's capacity = %d\n",cap);
	printf("Highest possible value=%d\n",best.max);

	for(int j=0;j<=i-1;j++)						//print the items used to attain max value
	{
		printf("Item %d (%s) : %d\n",j,allitems[j].name,best.items[j]);
	}

	return 0;
}
